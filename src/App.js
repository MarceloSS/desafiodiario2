import './App.css';

function App() {
  
  const DrinkList = ["Suco de laranja", "Suco de uva", "Suco de caju"]
  const AvailableSizeList = ["200ml", "300ml", "400ml"]

  return (
    <div className="App">
        <header className="App-header">
          <div>
            <div><h1>Bebidas</h1></div>

            {DrinkList.map((drink, index) => (
              <div key={index}> {drink} </div>
            ))}

          </div>

          <div>
            <div><h1>Tamanhos Disponiveis</h1></div>

            {AvailableSizeList.map((size, index) => (
              <li key={index}> {size} </li> 
            ))}  

          </div>

        </header>
    </div>
  );
}

export default App;
